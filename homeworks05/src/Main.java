

import java.util.Scanner;

public class HomeWorks05 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int digit = 0;
        int mindigit = 10;
        while (a != -1) {

            while (a != 0) {
                int lastDigit = a % 10;
                digit = lastDigit;
                a = a / 10;
                if (digit < mindigit) {
                    mindigit = digit;

                }


            }
            a = scanner.nextInt();

        }

        System.out.println(mindigit);
    }

