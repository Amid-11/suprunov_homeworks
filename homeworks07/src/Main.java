import java.util.*;
public class Main {
        public static void main(String[] args)  {
            int[] array = {-100, -100, -63,  -63, -75, -75,  20, 20, 20, 20, 21, -75, -63, -63, 100, 100, 100, 100, 100, 100};
            int result = 0;
            for (int i = 0; i < array.length; i++) {
                int number = array[i];
                if(isNumberNotProcessed(number, array, i)) {
                    result += calculateCount(number, array, i);
                }
            }
            System.out.println(result);
        }

        public static int calculateCount(int number, int[] array, int i) {
            int count = 1;
            for (int j = i + 1; j < array.length; j++) {
                if(array[j] == number) {
                    count++;
                }
            }
            if (count < 3) {
                System.out.println("число " + number +  " встречается меньше всех, то есть "  +count + " раз");
            }
            return  count;
        }

        public static boolean isNumberNotProcessed(int number, int[] array, int i) {
            if(i > 0) {
                for (int j = i - 1; j >= 0; j--) {
                    if(array[j] == number) {
                        return false;
                    }
                }
            }
            return true;

        }
    }

