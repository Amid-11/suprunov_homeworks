import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Human marsel = new Human();
        marsel.setWeight(44);
        marsel.setName("Марсель");


        Human victor = new Human();
        victor.setWeight(55);
        victor.setName("Виктор");

        Human robert = new Human();
        robert.setWeight(94);
        robert.setName("Роберт");

        Human ivan = new Human();
        ivan.setWeight(99);
        ivan.setName("Иван");

        Human jek = new Human();
        jek.setWeight(102);
        jek.setName("Джек");


        Human mark = new Human();
        mark.setWeight(93);
        mark.setName("Марк");

        Human jan = new Human();
        jan.setWeight(50);
        jan.setName("Жан");

        Human jora = new Human();
        jora.setWeight(88);
        jora.setName("Жора");

        Human juan = new Human();
        juan.setWeight(83);
        juan.setName("Хуан");

        Human oleg = new Human();
        oleg.setWeight(125);
        oleg.setName("Олег");




        System.out.println(marsel.getName() + " " + marsel.getWeight());
        System.out.println(victor.getName() + " " + victor.getWeight());
        System.out.println(robert.getName() + " " + robert.getWeight());
        System.out.println(ivan.getName() + " " + ivan.getWeight());
        System.out.println(jek.getName() + " " + jek.getWeight());
        System.out.println(mark.getName() + " " + mark.getWeight());
        System.out.println(jan.getName() + " " + jan.getWeight());
        System.out.println(jora.getName() + " " + jora.getWeight());
        System.out.println(juan.getName() + " " + juan.getWeight());
        System.out.println(oleg.getName() + " " + oleg.getWeight());


    }

}

