import homeworks09.*;

public class Main {

    public static void main(String[] args) {

	Figure figure = new Figure(10, 15);
    Ellipse ellipse = new Ellipse(3, 4);
    Square square = new Square(3);
    Rectangle rectangle = new Rectangle(3, 4);
    Circle circle = new Circle(3);

    System.out.println(Figure.getPerimeter());
    System.out.println(Ellipse.getPerimeter());
    System.out.println(Square.getPerimeter());
    System.out.println(Rectangle.getPerimeter());
    System.out.println(Circle.getPerimeter());




    }
}
