package homeworks09;

public class Square extends Rectangle {


    public Square(int x, int y) {
        super(x, y);
    }

    public Square(int x) {
        super(x);
    }


    public static double getPerimeter() {
        return x * 4;
    }
}
