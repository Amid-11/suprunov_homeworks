package homeworks10;

public class Circle extends Figure implements Move {
    public Circle(int x, int y) {
        super(x, y);
    }

    @Override
    public boolean getMove(int x, int y) {
        this.x = x;
        this.y = y;
        return false;
    }

    @Override
    public String toString() {
        return "Circle" +" x = " + x +", y = " + y +'.';
    }
}


