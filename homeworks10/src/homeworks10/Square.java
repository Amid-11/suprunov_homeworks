package homeworks10;

public class Square extends Figure implements Move {
    public Square(int x, int y) {
        super(y, y);
    }


    @Override
    public boolean getMove(int x, int y) {
        this.x = x;
        this.y = y;
        return false;
    }

    @Override
    public String toString() {
        return "Square" +" x = " + x +", y = " + y +'.';
    }
}

