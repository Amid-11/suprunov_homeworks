public class Logger {
    private String message;
// статическое глобальное (глобальное) поле, которое хранит единственный экземпляр класса. final не дает поменять ссылку на объект
    private static final Logger instance;
// статический инициализатор, который создает единственный объект класса
    static {
        instance = new Logger();
    }
//     приватный конструктор - запрещает создание объектов
    private Logger(String message) {
        this.message = message;
    }


// метод для получения единственного объекта
    public  static Logger getInstance() {
        return instance;
    }

    public Logger() {
    }
// методо void log(String message), который выводит в консоль какое-либо сообщение
    public static void log(String message) {
        System.out.println(message);
    }

    @Override
    public String toString() {
        return "Logger{" +
                "message='" + message + '\'' +
                '}';
    }
}
